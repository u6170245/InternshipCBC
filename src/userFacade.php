<?

// a facade based on event api and plugin

/**
* get_user
* GET /users/:id/
*        Returns a :format:`user` for the specified user as ``user``. If you want to get details about the currently authenticated user, use ``/users/me/``.
*/
public function get_user($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/", $id), $expand=$expand);
}


/**
* get_user_owned_events
* GET /users/:id/owned_events/
*        Returns a :ref:`paginated <pagination>` response of :format:`events <event>`, under
*        the key ``events``, of all events the user owns (i.e. events they are organising)
*/
public function get_user_owned_events($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/owned_events/", $id), $expand=$expand);
}


/**
* get_user_events
* GET /users/:id/events/
*        Returns a :ref:`paginated <pagination>` response of :format:`events <event>`, under the key ``events``, of all events the user has access to
*/
public function get_user_events($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/events/", $id), $expand=$expand);
}


/**
* get_user_venues
* GET /users/:id/venues/
*        Returns a paginated response of :format:`venue` objects that are owned by the user.
*/
public function get_user_venues($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/venues/", $id), $expand=$expand);
}


/**
* get_user_owned_event_attendees
* GET /users/:id/owned_event_attendees/
*        Returns a :ref:`paginated <pagination>` response of :format:`attendees <attendee>`,
*        under the key ``attendees``, of attendees visiting any of the events the user owns
*        (events that would be returned from ``/users/:id/owned_events/``)
*/
public function get_user_owned_event_attendees($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/owned_event_attendees/", $id), $expand=$expand);
}


/**
* get_user_owned_event_orders
* GET /users/:id/owned_event_orders/
*        Returns a :ref:`paginated <pagination>` response of :format:`orders <order>`,
*        under the key ``orders``, of orders placed against any of the events the user owns
*        (events that would be returned from ``/users/:id/owned_events/``)
*/
public function get_user_owned_event_orders($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/owned_event_orders/", $id), $expand=$expand);
}


/**
* get_user_contact_lists
* GET /users/:id/contact_lists/
*        Returns a list of :format:`contact_list` that the user owns as the key
*        ``contact_lists``.
*/
public function get_user_contact_lists($id, $expand=array()) {
    return $this->get(sprintf("/users/%s/contact_lists/", $id), $expand=$expand);
}


/**
* post_user_contact_lists
* POST /users/:id/contact_lists/
*        Makes a new :format:`contact_list` for the user and returns it as
*        ``contact_list``.
*/
public function post_user_contact_lists($id, $data=array()) {
    return $this->post(sprintf("/users/%s/contact_lists/", $id), $data=$data);
}


/**
* get_user_contact_list
* GET /users/:id/contact_lists/:contact_list_id/
*        Gets a user's :format:`contact_list` by ID as ``contact_list``.
*/
public function get_user_contact_list($id, $contact_list_id, $expand=array()) {
    return $this->get(sprintf("/users/%s/contact_lists/%s/", $id, $contact_list_id), $expand=$expand);
}


/**
* post_user_contact_list
* POST /users/:id/contact_lists/:contact_list_id/
*        Updates the :format:`contact_list` and returns it as ``contact_list``.
*/
public function post_user_contact_list($id, $contact_list_id, $data=array()) {
    return $this->post(sprintf("/users/%s/contact_lists/%s/", $id, $contact_list_id), $data=$data);
}


/**
* delete_user_contact_list
* DELETE /users/:id/contact_lists/:contact_list_id/
*        Deletes the contact list. Returns ``{"deleted": true}``.
*/
public function delete_user_contact_list($id, $contact_list_id, $data=array()) {
    return $this->delete(sprintf("/users/%s/contact_lists/%s/", $id, $contact_list_id), $data=$data);
}


/**
* get_user_contact_lists_contacts
* GET /users/:id/contact_lists/:contact_list_id/contacts/
*        Returns the :format:`contacts <contact>` on the contact list
*        as ``contacts``.
*/
public function get_user_contact_lists_contacts($id, $contact_list_id, $expand=array()) {
    return $this->get(sprintf("/users/%s/contact_lists/%s/contacts/", $id, $contact_list_id), $expand=$expand);
}


/**
* post_user_contact_lists_contacts
* POST /users/:id/contact_lists/:contact_list_id/contacts/
*        Adds a new contact to the contact list. Returns ``{"created": true}``.
*        There is no way to update entries in the list; just delete the old one
*        and add the updated version.
*/
public function post_user_contact_lists_contacts($id, $contact_list_id, $data=array()) {
    return $this->post(sprintf("/users/%s/contact_lists/%s/contacts/", $id, $contact_list_id), $data=$data);
}


?>
