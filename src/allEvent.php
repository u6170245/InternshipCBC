<?php

$url = 'https://www.eventbriteapi.com/v3/users/me/owned_events/';
$token = 'XUEXBVP2PMW7CBCPVBLJ';

$opts = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Authorization: Bearer XUEXBVP2PMW7CBCPVBLJ\r\n"
	)
);

$optsNokey = array(
	'http'=>array(
		'method'=>"GET"
	)
);


// Method 1. using Authorization
function method1() {
	global $url, $token, $opts, $optsNokey;
	$context = stream_context_create($opts);
	$result = file_get_contents($url, false, $context);

	$response = json_decode($result,true);
	print_r($response);
}


// Method 2. using Get parameter
function method2() {
	global $url, $token, $opts, $optsNokey;
	$context = stream_context_create($optsNokey);
	$result = file_get_contents($url."?token=".$token, false, $context);

	$response = json_decode($result,true);
	print_r($response);
}


// Method 3. use another file operation
function method3() {
	global $url, $token, $opts, $optsNokey;
	$context = stream_context_create($opts);
	$fp = fopen($url, 'r', false, $context);
	fpassthru($fp);
	fclose($fp);
}

//method1();
//method2();
method3();


?>
