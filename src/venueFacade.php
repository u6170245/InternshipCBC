<?php

// a facade based on eventbrite api and plugin


/**
* get_venue
* GET /venues/:id/
*        Returns a :format:`venue` object.
*/
public function get_venue($id, $expand=array()) {
    return $this->get(sprintf("/venues/%s/", $id), $expand=$expand);
}


/**
* post_venue
* POST /venues/:id/
*        Updates a :format:`venue` and returns it as an object.
*/
public function post_venue($id, $data=array()) {
    return $this->post(sprintf("/venues/%s/", $id), $data=$data);
}


/**
* post_venues
* POST /venues/
*        Creates a new :format:`venue` with associated :format:`address`.
*        ..start-internal
*/
public function post_venues($data=array()) {
    return $this->post(sprintf("/venues/"), $data=$data);
}


/**
* get_venues_search
* GET /venues/search/
*        Search for venues. Returns a list of venue objects.
*        ..end-internal
*/
public function get_venues_search($expand=array()) {
    return $this->get(sprintf("/venues/search/"), $expand=$expand);
}


/**
* get_venues_events
* GET /venues/:id/events/
*        Returns events of a given :format:`venue`.
*/
public function get_venues_events($id, $expand=array()) {
    return $this->get(sprintf("/venues/%s/events/", $id), $expand=$expand);
}

?>
