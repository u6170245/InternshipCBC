<? php

// a facade based on eventbrite api and plugin

/**
* get_event_search
* GET /events/search/ 
*        Allows you to retrieve a paginated response of public :format:`event` objects from across Eventbrite’s directory, regardless of which user owns the event.
*/
public function get_event_search($expand=array()) {
    return $this->get(sprintf("/events/search"), $expand=$expand);
}


/**
* post_events
* POST /events/
*        Makes a new event, and returns an :format:`event` for the specified event. Does not support the creation of repeating
*        event series.
*        field event.venue_id
*        The ID of a previously-created venue to associate with this event.
*        You can omit this field or set it to ``null`` if you set ``online_event``.
*/
public function post_events($data=array()) {
    return $this->post(sprintf("/events/"), $data=$data);
}


/**
* get_event
* GET /events/:id/
*        Returns an :format:`event` for the specified event. Many of Eventbrite’s API use cases revolve around pulling details
*        of a specific event within an Eventbrite account. Does not support fetching a repeating event series parent
*        (see :ref:`get-series-by-id`).
*/
public function get_event($id, $expand=array()) {
    return $this->get(sprintf("/events/%s/", $id), $expand=$expand);
}


/**
* post_event
* POST /events/:id/
*        Updates an event. Returns an :format:`event` for the specified event. Does not support updating a repeating event
*        series parent (see POST /series/:id/).
*/
public function post_event($id, $data=array()) {
    return $this->post(sprintf("/events/%s/", $id), $data=$data);
}


/**
* post_event_publish
* POST /events/:id/publish/
*        Publishes an event if it has not already been deleted. In order for publish to be permitted, the event must have all
*        necessary information, including a name and description, an organizer, at least one ticket, and valid payment options.
*/
public function post_event_publish($id, $data=array()) {
    return $this->post(sprintf("/events/%s/publish/", $id), $data=$data);
}


/**
* post_event_unpublish
* POST /events/:id/unpublish/
*        Unpublishes an event. In order for a free event to be unpublished, it must not have any pending or completed orders,
*        even if the event is in the past. In order for a paid event to be unpublished, it must not have any pending or completed
*        orders, unless the event has been completed and paid out. Returns a boolean indicating success or failure of the
*        unpublish.
*/
public function post_event_unpublish($id, $data=array()) {
    return $this->post(sprintf("/events/%s/unpublish/", $id), $data=$data);
}


/**
* post_event_cancel
* POST /events/:id/cancel/
*        Cancels an event if it has not already been deleted. In order for cancel to be permitted, there must be no pending or
*        completed orders. Returns a boolean indicating success or failure of the cancel.
*/
public function post_event_cancel($id, $data=array()) {
    return $this->post(sprintf("/events/%s/cancel/", $id), $data=$data);
}


/**
* delete_event
* DELETE /events/:id/
*        Deletes an event if the delete is permitted. In order for a delete to be permitted, there must be no pending or
*        completed orders. Returns a boolean indicating success or failure of the delete.
*/
public function delete_event($id, $data=array()) {
    return $this->delete(sprintf("/events/%s/", $id), $data=$data);
}


/**
* get_event_ticket_classes
* GET /events/:id/ticket_classes/
*        Returns a :ref:`paginated <pagination>` response with a key of
*        ``ticket_classes``, containing a list of :format:`ticket_class`.
*/
public function get_event_ticket_classes($id, $expand=array()) {
    return $this->get(sprintf("/events/%s/ticket_classes/", $id), $expand=$expand);
}


/**
* post_event_ticket_classes
* POST /events/:id/ticket_classes/
*        Creates a new ticket class, returning the result as a :format:`ticket_class`
*        under the key ``ticket_class``.
*/
public function post_event_ticket_classes($id, $data=array()) {
    return $this->post(sprintf("/events/%s/ticket_classes/", $id), $data=$data);
}


/**
* get_event_ticket_class
* GET /events/:id/ticket_classes/:ticket_class_id/
*        Gets and returns a single :format:`ticket_class` by ID, as the key
*        ``ticket_class``.
*/
public function get_event_ticket_class($id, $ticket_class_id, $expand=array()) {
    return $this->get(sprintf("/events/%s/ticket_classes/%s/", $id, $ticket_class_id), $expand=$expand);
}


/**
* post_event_ticket_class
* POST /events/:id/ticket_classes/:ticket_class_id/
*        Updates an existing ticket class, returning the updated result as a :format:`ticket_class` under the key ``ticket_class``.
*/
public function post_event_ticket_class($id, $ticket_class_id, $data=array()) {
    return $this->post(sprintf("/events/%s/ticket_classes/%s/", $id, $ticket_class_id), $data=$data);
}


/**
* delete_event_ticket_class
* DELETE /events/:id/ticket_classes/:ticket_class_id/
*        Deletes the ticket class. Returns ``{"deleted": true}``.
*/
public function delete_event_ticket_class($id, $ticket_class_id, $data=array()) {
    return $this->delete(sprintf("/events/%s/ticket_classes/%s/", $id, $ticket_class_id), $data=$data);
}


/**
* get_event_attendees
* GET /events/:id/attendees/
*        Returns a :ref:`paginated <pagination>` response with a key of ``attendees``, containing a list of :format:`attendee`.
*/
public function get_event_attendees($id, $expand=array()) {
    return $this->get(sprintf("/events/%s/attendees/", $id), $expand=$expand);
}


/**
* get_event_attendee
* GET /events/:id/attendees/:attendee_id/
*        Returns a single :format:`attendee` by ID, as the key ``attendee``.
*/
public function get_event_attendee($id, $attendee_id, $expand=array()) {
    return $this->get(sprintf("/events/%s/attendees/%s/", $id, $attendee_id), $expand=$expand);
}


/**
* get_event_orders
* GET /events/:id/orders/
*        Returns a :ref:`paginated <pagination>` response with a key of ``orders``, containing a list of :format:`order` against this event.
*/
public function get_event_orders($id, $expand=array()) {
    return $this->get(sprintf("/events/%s/orders/", $id), $expand=$expand);
}

?>
