<?php

// a facade based on event api and plugin

/**
* get_media
* GET /media/:id/
*        Return an :format:`image` for a given id.
*        .. _get-media-upload:
*/
public function get_media($id, $expand=array()) {
    return $this->get(sprintf("/media/%s/", $id), $expand=$expand);
}


/**
* get_media_upload
* GET /media/upload/
*        See :ref:`media-uploads`.
*        .. _post-media-upload:
*/
public function get_media_upload($expand=array()) {
    return $this->get(sprintf("/media/upload/"), $expand=$expand);
}


/**
* post_media_upload
* POST /media/upload/
*        See :ref:`media-uploads`.*/
public function post_media_upload($data=array()) {
    return $this->post(sprintf("/media/upload/"), $data=$data);
}


?>
