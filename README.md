# Welcome
I am Hongbo Zhang, u6170245.
I think you have already known these if you look at the URL of this webpage.

Welcome to the repo of my internship at Canberra Business Chamber (CBC).

# About the Company
Canberra Business Chamber (CBC) is a non-profit private company.

CBC is like a labor union, but for companies. It provides many kinds of services to its members (companies), and it communicates with government and other organizations as the representative of the members as well.

It supports business growth by connecting, advising and educating the members and by advocating for policy and investment decisions which will facilitate economic development in the Canberra region [(1)](https://www.canberrabusiness.com/). And it is the leading voice of business in a growing regional economy with increasing global reach [(1)](https://www.canberrabusiness.com/).


# About the Project

CBC has signed a contract with government.
In that contract, CBC is required to build its own trading calendar, 
so that it will promote CBC becoming a leader in trading business.

In a trading calendar, everyone, rather than CBC and its member only,
can create, save, edit and submit an event related to international
 trade which is held in Australia (not restricted to Canberra),
 including workshop, seminar, networking, webinar and so on. 
After submitting, staff at CBC will approve, update or reject
the event. However, if approved, this event will be put on
trading event website, and anyone can search them.

## Old Project 
The scope of this project has been changed to previous one.

~~CBC makes extensive use of third party IT services to facilitate their business.
For example, they use Infusionsoft to deal with their CRM, and Xero as their financial system. 
However, these third party services don't talk to each other.  
It will bring inconenvient to their business.
Especially, when the members pay their membership fee annually, 
staff of CBC has to export their member info from CRM system, and input them to financial system manually.~~

~~In this context, the aim of this project is to make the above process automatically, and bring value to CBC.~~

	
# About the Plan

Figure below is in low resolution and is just a snapshot. For vector figure, 
see the pdf format [here](/docs/design/userStory/userMap170823p.pdf).

![user map page1](/docs/design/userStory/snap/userStory170823p1.png)
![user map page2](/docs/design/userStory/snap/userStory170823p2.png)

~~User map of old project, see [here](/docs/design/oldProject/userMap1.png)~~

# About the Tech Stack

+ LAMP architecture
+ Front end. HTML, CSS, JS
+ Back end.
	- Content management system, WordPress. Build a WordPress plugin to satisfy the requirement of client.
	- Application Programming Interface, eventbrite API v3
	- Other application by PHP
+ Database.
	- MySQL
	- Data Service from EventBrite.

# Stakeholders

Hongbo (Me), Peter (Tutor), Alaine (Mentor), Amanda (Co-worker), Shanye and Ramesh (Prof.)

Erika, Steven and Dinko (Steering Committee)

# Assessment Items

##  1. Decision Making Evidence

+ In July 26, Amanda and I check the availability of Infusionsoft and Xero API and feasibility of Auto-Invoice project. Finally, we decided to accept this project. Details see [here](/docs/dairy/README.md#jul-26)
+ In July 31, I discuss with Erika, and made a decision that this projects will be finished by delivering 3 releases. Details see [here](/docs/dairy/README.md#jul-31)
+ From Aug 2 to 14, CBC decided that intern should not have permission to log in their financial system. I have to change the scope of project. Details see [here](/docs/dairy/README.md#aug-2)
+ In Aug 14, between two possibilities (build Auto-Invoice in sandbox and Event-Calendar project), I decided to do the Event-calendar project. Since the first one can only be done in testing environment and there is already existed an on-shelf plugin for the first one. Details see [here](/docs/dairy/README.md#aug-14)
+ In Aug 23, Erika and I extract MVP from user stories. Details see [here](/docs/dairy/README.md#aug-23)
+ ~~From Aug 15 to 28, I am making a decision that which technical path should I take (build from sctrach, or build this calendar on eventbrite, or ...). Haven't finally decided yet.~~
+ In Sep 18, Erika will leave CBC in next week. So my supervisor changes to Steven and Dinko. Steven responds for the project, and Dinko responds for user experience. Details see [here](/docs/dairy/README.md#sep-18)
+ In Sep 20, I check all EventBrite API and make decision to build CBC international trading calendar on the system of EventBrite. Eventbrite provides almost all the API I need at first release (except sending email when some action happens). Decision making process can be found at [here](/docs/dairy/README.md#sep-20) and [here](/docs/design/tech/eventbriteAPI.md). List of relevant API can be found [here](/docs/dairy/README.md#sep-20).
+ In Oct 4, I decide to adopt the workflow of using a Personal Token, rather than OAuth Token flow. This will make things simpler and already satisfy the requirement of CBC. Details see [here](/docs/dairy/README.md#oct-4)
+ In Oct 4, I decide to use the SDK provided by Eventbrite offically, rather than the Wordpress plugin. Since the WordPress plugin only provides three endpoints (`user_owned_events, event_details` and `event_search`, which are not enough for our [purpose](/docs/dairy/README.md#sep-20). 
+ In Oct 9, I discuss with Steven on the issue of payment. We finally reach a conclusion that users of this CBC event calendar will submit their bank account info when they create an event. Detailed issue and solution can be found [here](/docs/dairy/README.md#oct-9).
+ In Oct 9, Steven doesn't allow me to make the code open source, unless hacks will not hack the system by reading the code. Therefore, I decide not to upload source code here. Details see [here](/docs/dairy/README.md#oct-9).

## 2. About the Structure of the Repo
+ ***working diary*** is [here](/docs/dairy). Record every event, dicussion, decision making, info and so on in details.
+ design
	+ all the documents related to ***the user map*** is [here](/docs/design/userStory/)
	+ ***technical path***. see [here](/docs/design/tech/eventbriteAPI.md). ~~hasn't been finalised yet.~~
	+ ***architecture design***. ~~haven't been finalised at this stage. maybe LAMP, maybe js + 3rd party services.~~  LAMP architecture. See [here](/docs/design/tech/stack.md)
	+ ***domain/UML design***. haven't begun at this stage, to been added in the future.
+ all the ***source code*** can be found [here](/src) ~~if CBC permits to share.~~ CBC dosn't allow to upload code here, unless hackers will not hack the website by reading the source code. So there is only testing code here.
+ ***agenda***. to be added in the future.
+ reviews
	+ the ***first review***, concept review, is [here](/docs/review/conceptReview.pdf)
	+ the ***second review*** is [here](/docs/review/secondReview.pdf)
	+ the ***third review*** is [here](/docs/review/final.pdf)
	+ the ***poster*** is [here](/docs/review/poster_a2.pdf)

## 3. Traceability of action

Every event, discuss, decision making, information, working progress and so on are recorded in every details in ***working diary*** ([click here](/docs/dairy/)), ***design directory***([click here](/docs/design/))


## 4. Outputs

### ~~Auto-Invoice Project~~
+ ~~Backbone, skeleton, and release plan of Auto-invoice project. See [here](/docs/design/oldProject/userMap1.png)~~
+ ~~Quick sketch of user experience interface. See [here](/docs/design/oldProject/IMG_2157.JPG)~~
+ ~~Quick sketch of architecture design. See [here](/docs/design/oldProject/IMG_2158.JPG)~~
+ ~~However, the Auto-invoice project terminated, so all the output is just preliminary~~

### Event-Calendar Project.
+ Roles of all users. See [here](/docs/design/userStory/roles.md)
+ User stories of Event-Calendar project. See [here](/docs/design/userStory/story170816.pdf)
+ User Map and First Release. See [here](/docs/design/userStory/userMap170823p.pdf)
+ ~~Up to Aug-28, we are at the stage of user story validation and technical path selection, so all the output are related to product design, rather than implementation. Implementation will come after story validation.~~
+ Poster. See [here](/docs/review/poster_a2.pdf)
+ Technical path analysis. See [here](/docs/design/tech/eventbriteAPI.md)
+ Eventbrite API analysis. See [here](/docs/design/tech/eventbriteAPI.md)
+ Testing code. See [here](/src/)
+ *Still under implementation*

## 5. Development Approach
We use scrum methodology to manage the project. All the relevant progress of project management can be found in working diary.
~~For the technical path, we haven't make final decision up to Aug-28.~~

## 6. Engagement with Stakeholders

+ regular office hour. Every Monday 9am to 4pm and Wednesday 9am to 5pm, I work in office of CBC. And I work at home for 1 hour every week.
+ events
	- Jul 26. scope meeting of Auto-invoice project. Details see [here](/docs/dairy/README.md#jul-26)
	- Jul 31. scope meeting of Auto-invoice project in more details.Details see [here](/docs/dairy/README.md#jul-31)	
	- Aug 7, concept review. Details see [here](/docs/dairy/README.md#aug-7)
	- Aug 9, mentor meeting. Details see [here](/docs/dairy/README.md#aug-9)
	- Aug 14, scope meeting of Event-calendar project with mentor, CBC (Erika, Kate) and me. Details see [here](/docs/dairy/README.md#aug-14) 
	- Aug 15, meet with Shayne and Ramesh to the feasibility my CBC project. Details see [here](/docs/dairy/README.md#aug-15)
	- Aug 16, story workshop with Erika. Details see [here](/docs/dairy/README.md#aug-16)
	- Aug 18, mentor meeting, help me on how to make decision on the technical path. Details see [here](/docs/dairy/README.md#aug-18)
	- Aug 23, story workshop with Erika. Details see [here](/docs/dairy/README.md#aug-23)
	- Aug 28, drop in the IT support department of ANU. Details see [here](/docs/dairy/README.md#aug-28)
	- Sep 18, meet Steven and Dinko. Introduce the project briefly with them. Details see [here](/docs/dairy/README.md#sep-18)
	- Oct 3, discuss with Alaine on the structure of poster. Details see [here](/docs/dairy/README.md#oct-3)
	- Oct 9, discuss with Steven about whether can I make the source code open source. Details see [here](/docs/dairy/README.md#oct-9)
	- Oct 9, discuss with Steven about the issue of payment. Details see [here](/docs/dairy/README.md#oct-9)
	- Oct 15, Alaine revise my CV.

## 7. Professional Team Attitude and Collaboration
This is an individual intern project. My supervisor (client)
is my only teammate in workplace. I collaborate with her extensively,
which can be traced from subsection *Egnagement with Stakeholders* above this
subsection.

## 8. Acting on Feedback
+ I update the whole landing page in every details. 
+ Add Stakeholders
+ Add tech stacks
+ In decision making, I include more details, such as listing all the stakeholders who made decision, rather than using "we" only.
+ Add every details, including decision-making and all technical details in diary.
