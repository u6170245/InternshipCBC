# Two Possible Technical Path

1. build everything from scratch. cannot finish in 3 month, client is difficult  to maintain it, expensive to maintain.
2. integrate CBC with eventbrite. less expensive in time and money, low maintain cost, eventbrite update, client will get that value automatically, less time to market, proven and widely accepted product. lots of functions provided by eventbrite, such as invitation, pay for tickets, google map, FAQ, multiple date event ...(can be integrated to CBC later).***using this one***

# Two Possible Solution to Integrate CBC with Eventbrite

1. CBC create a unique account, and all the users of this website will publish their events on eventbrite using this unique account
	1. Advantages. CBC has more control 
	2. Disadventage. if paid on eventbrite, fee will first go to CBCEC account, then CBCEC transfers money to their users

2. Any user who wants to publish an event will register an account on eventbrite, and CBC will decide which event is displayed on CBC's website.
	1. Advantages. paid function will work well.
	2. Disadventage. less control by CBC

# Minimum Viable Product and EventBrite API

This part focus on the first possibility of integrating CBC with eventbrite.
CBC event calendar registers a unique account, namely CBCEC, at eventbrite.

Eventbrite API see [here](https://www.eventbrite.com/developer/v3/#ebapi-endpoints)

## About register

+ when a user registers an account, we write them into a txt file on server. 
+ It is not necessary to use DBMS at present stage, since the number of users should not too much. (there are only 600 members for CBC, even we amplify this number by 100, it is 60 thousands, which is not too much at all)
+ if we use DBMS, we have to install DBMS on server of CBC. However, their server is maintained by 3rd party company. It will be inconenvient for us to config, develop, test, debug. 
+ As well, it will cost CBC money to buy data storage service. But currently, this is only a prototype. 
+ If the number of users increase greatly, we will transfer to DBMS at that time.

## About login

+ when a user logs in, check whether he is in txt file.


## About update user profile

+ update txt file

## About creating event

+ fill in a form
+ `POST /events/` for creating new event

## About viewing event

+ list all events of CBCEC with specific event organiser fields.
+ `https://www.eventbriteapi.com/v3/users/me/owned_events/?token=MYTOKEN1`
+ there is a field `status` in responds json to tell whether is live or not. For us, it tells an event if submitted or approved

## About updating event

+ in viewing event page, user clicks an update button on an event
+ he can fill in a form. 
+ then submit this form, and change correspondence fields in eventbrite event.
+ `POST /event/:id/` to update an event

## About deleting event

+ in viewing event page, user clicks a delete button on an event.
+ he can delete an event
+ `POST /events/:id/cancel/`
+ `DELETE /event/:id/`

## About submit

+ when click submit button, this form is ***submitted to eventbrite by account CBCEC*** without "Make Event Live" 
+ `POST /events/:id/publish/` 
+ CBC admin should set it to public, rather than private.

## About emails when any operation (submit, update, approve, reject) happens

+ TODO does eventbrite have this function? eventbrite only has invitation function when the an event goes alive.

## About viewing and searching events by attendee

+ viewing all events of CBCEC.
+ `https://www.eventbriteapi.com/v3/users/me/owned_events/?token=MYTOKEN1` with `status` doesn't euqal to `live`

## About managing events by Admin

+ just login eventbrite by CBCEC, and do stuff



