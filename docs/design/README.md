# Structure of this directory

+ [oldProject](./oldProject) is the design folder for old project, saying making invoice generating process automatically.
+ [userStory](./userStory) contains all the design documents about user story map.
+ [tech](./tech) contains all the documents related to technicial path.
