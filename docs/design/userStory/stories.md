This file has been decomposited into two files, mvp170823.md and nonMVP170823.md.

New stories are added in nonmvp170823.md from now on, and this file is no longer maintained.
So using that file for stories in the future.

# 1. Eventer

## Board requirements

+ As an Eventer/Attendant, I want to use this website by webpage, so that
+ As an Eventer/Attendant, I want to use this website by phone, so that

## registeration
+ As an Eventer, I want to register to trade calendar by username, so that  
+ As an Eventer, I want to register to trade calendar by email, so that  
+ As an Eventer, I want to register to trade calendar by facebook, so that  

+ As an Eventer, I want to initiate my profile immediately after registeration, so that
	- profile should include email(compulsory), tel, addr, company/organization, links to company (optional)
	- just contact details, no something like slogan, CBC don't advertise Eventer's business
+ As an Eventer, I want to watch a quick tutorial on how to use this website, so that
+ As an Eventer, I want to get back my password if I forget via email, so that

## login/out
+ As an Eventer, I want to login the system by username and pwd, so that
+ As an Eventer, I want to login the system by email and pwd, so that
+ As an Eventer, I want to login the system by facebook and pwd, so that
+ As an Eventer, I want to logout the system, so that

+ As an Eventer, I want to update my profile, so that

## edit event
there are three states of events. unsubmitted, submitted, approved.

### create
+ As an Eventer, I want to create an empty event, so that
+ As an Eventer, I want to create an event from a template, so that
	- saving company name, logo in template

### read
+ As an Eventer, I want to view a list of my approved events, so that
+ As an Eventer, I want to view a list of my unsubmitted events, so that
+ As an Eventer, I want to view a list of my submitted events, so that
+ As an Eventer, I want to view a list of all my events, so that

### update
+ As an Eventer, I want to edit an existed but unsubmitted event, so that
	- event type, time, location, simple description, image/logo, link to registeration page, link to company, contact details, cost
+ As an Eventer, I want to save an unsubmitted event, so that I can edit it later
+ As an Eventer, I want to update a submitted event and notifying CBC, so that
+ As an Eventer, I want to update an approved event, so that
	- only date, location, contact info, link, logo, description, cost are allowed
	- the company cannot be changed by Eventer. in this case, he should contact CBC

### delete
+ As an Eventer, I want to delete an unsubmitted event, so that
+ As an Eventer, I want to withdraw a submitted event, so that
+ As an Eventer, I want to cancel an approved event, so that

## submit
+ As an Eventer, I want to submit my event, so that
+ As an Eventer, I want to get notification when the event is approved, so that
+ As an Eventer, I want to get notification if the event is rejected, so that 
+ As an Eventer, I want to additional comments in notification if the event is rejected, so that 

## contact CBC
+ As an Eventer, I want to contact with CBC if I got something wrong, so that
	- a webpage to allow Eventer writing emails to CBC

# 2. Attendee

## view 
+ As an Attendee, I want to view all events in the form of list ordered by date and at most 10 event per page, so that
+ As an Attendee, I want to view all events in the form of calendar, so that
	- Actually, not in form of calendar, just a calendar sit on the top of website, so that anyone can select a specific date to view
+ As an Attendee, I want to know info about the event, so that
	- blurb. event name, type, time, blurb (20 words limited), a link to detail info page
	- detail info page. event name, type, time, location, simple description, image/logo, linkes, contact details, company, cost

## search
+ As an Attendee, I want to search events by ..., so that
	- month, date, (exactly, or in a range), company
	- type, state location 
+ As an Attendee, I want to fuzzy search event by ..., so that
	- simple description
+ As an Attendee, I want to get recommendation of "hottest" event, so that


# 3. Administrator (more than 1 admin email, could be only one admin account)

## event handling
+ As an Admin, I want to get notification when an event is submitted by email, so that
+ As an Admin, I want to review an submitted event, so that
+ As an Admin, I want to approve an submitted event after reviewing, so that
+ As an Admin, I want to approve an submitted event without review, so that
+ As an Admin, I want to cancel a submitted event, so that (maybe useless, just reject is OK)
+ As an Admin, I want to cancel an approved event, so that
+ As an Admin, I want to reject a submitted event without any comment, so that
+ As an Admin, I want to reject a submitted event with additional comment, so that
+ As an Admin, I want to update an submitted event, so that (no context to use this function)
+ As an Admin, I want to update an approved event, so that Eventer can change the company name if there is a mistake

## website managing
+ As an Admin, I want to view a list of Eventers, so that
+ As an Admin, I want to export a list of Eventers, so that
+ As an Admin, I want to delete inactivity Eventer, so that
	- what is the definition of inactivity? not login for a long time, or not submit event for a long time
+ As an Admin, I want to view the log of all operation history record, so that
	- Eventer and Admin operations 
+ As an Admin, I want to export the log of all operation history record, so that
	- cvs
+ As an Admin, I want to generate report, so that ... (big story, no specific requirements at present, leave it aside)

## communications
+ As an Admin, I want to receive comments/queries from Eventer/Attendee via email, so that ...

## advanced task
+ As an Admin, I want to assign some event as "hot" event, so that 
+ As an Admin, I want to create/edit/save/approved event by myself, so that (can be done in other way, Erika registers an Eventer account)
+ As an Admin, I want to smart collect events from other related website, so that
	- [austrade](www.austrade.gov.au)
	- [trade event](www.tradeevents.australia.com)
	- [her canberra](http://hercanberra.com.au/)



