As a/an Event Owner I want to be able to create a log in with my email and a password So that I am able to come back and edit my event

As a/an Event Owner I want to be able to add my company logo to my event So that attendees area aware of who is running the event

As a/an Event Owner I want to be able to add a link for my event registration page So that I am able to manage the attendees myself 

As a/an Event Owner I want to be able to add a link to my company website So that attendees can access my company information if needed

As a/an Event Owner I want to be notified when my event has been approved or rejected So that I can check on how it looks on the CBC website

As a/an Event Owner I want to be able to edit my event after it has gone live on the CBC calendar So that I can amend any required changes if they come up
					

As a/an CBC Administrator I want to be notified when an Event Owner has completed entering an event So that I can review and approve the event

As a/an CBC Administrator I want be able to inform the Event Owner that their event has been rejected and an option to provide details on reason So that communications can be minimised

As a/an CBC Administrator I want to be able to retract an approval for an event So that we can have control over event calendar

As a/an CBC Administrator I want pull off a list of accounts created on event calendar So that so we can monitor and track users (comment by hongbo, if we are using eventbrite, this function may be not easy to implement)

As a/an CBC Administrator I want to be able to delete an account So that we can get rid of old or unused accounts
					

As a/an Event Attendee I want to be able to choose between vewing events in a list or calendar format So that viewing events can be easier

As a/an Event Attendee I want to be able to search events based on date and company So that 

As a/an Event Attendee I want to be able to scroll through a list of events in date order So that

 

 
