# Eventer

## Board requirements

+ As an Eventer/Attendant, I want to use this website by webpage, so that
+ As an Eventer/Attendant, I want to use this website by phone, so that

## registeration
+ As an Eventer, I want to register to trade calendar by username, so that  
+ As an Eventer, I want to register to trade calendar by email, so that  
+ As an Eventer, I want to register to trade calendar by facebook, so that  

+ As an Eventer, I want to initiate my profile immediately after registeration, so that
	- profile should include email(compulsory), tel, addr, company/organization, 
+ As an Eventer, I want to watch a quick tutorial on how to use this website, so that
+ As an Eventer, I want to get back my password if I forget via email, so that

## login/out
+ As an Eventer, I want to login the system by username, so that
+ As an Eventer, I want to login the system by email, so that
+ As an Eventer, I want to login the system by facebook, so that
+ As an Eventer, I want to logout the system, so that

+ As an Eventer, I want to update my profile, so that

## edit event
[](there-are-three-states-of-events.-unsubmitted,-submitted,-approved)
[](create)
+ As an Eventer, I want to create an empty event, so that
+ As an Eventer, I want to create an event from a template, so that
[](read)
+ As an Eventer, I want to view a list of my approved events, so that
+ As an Eventer, I want to view a list of my unsubmitted events, so that
+ As an Eventer, I want to view a list of my submitted events, so that
+ As an Eventer, I want to view a list of all my events, so that
[](update)
+ As an Eventer, I want to edit an existed but unsubmitted event, so that
	- event type, time, location, simple description, image/logo, linkes, contact details
+ As an Eventer, I want to save an unsubmitted event, so that I can edit it later
+ As an Eventer, I want to update a submitted event and notifying CBC, so that
+ As an Eventer, I want to update an approved event, so that
	- only date, location, contact info, link, logo, description are allowed
	- the company cannot be changed by Eventer. in this case, he should contact CBC
[](delete)
+ As an Eventer, I want to delete an unsubmitted event, so that
+ As an Eventer, I want to withdraw a submitted event, so that
+ As an Eventer, I want to cancel an approved event, so that

## submit
+ As an Eventer, I want to submit my event, so that
+ As an Eventer, I want to get notification when the event is approved, so that
+ As an Eventer, I want to get notification if the event is rejected, so that 
+ As an Eventer, I want to additional comments in notification if the event is rejected, so that 

## contact CBC
+ As an Eventer, I want to contact with CBC if I got something wrong, so that
	- a webpage to allow Eventer writing emails to CBC

# Attendee

## look through
+ As an Attendee, I want to read all events in a list ordered by data, so that
+ As an Attendee, I want to know info about the event, so that
	- event type, time, location, simple description, image/logo, linkes, contact details, company

## search
+ As an Attendee, I want to search events by ..., so that
	- month, date, (exactly, or in a range)
	- type, company, location 
+ As an Attendee, I want to fuzzy search event by ..., so that
	- simple description
+ As an Attendee, I want to get recommendation of "hottest" event, so that


# Administrator
+ As an Admin, I want to get notification when an event is submitted by email, so that
+ As an Admin, I want to approve an submitted event, so that
+ As an Admin, I want to cancel an submitted event, so that
+ As an Admin, I want to update an submitted event, so that (no context to use this function)
+ As an Admin, I want to update an approved event, so that Eventer can change the company name if there is a mistake
+ As an Admin, I want to view the log of all operation history record, so that
	- Eventer and Admin operations 
+ As an Admin, I want to export the log of all operation history record, so that
	- cvs
+ As an Admin, I want to receive comments/queries from Eventer/Attendee via email, so that ...
+ As an Admin, I want to assign some event as "hot" event, so that 

+ As an Admin, I want to generate report, so that ... (big story, no specific requirements at present, leave it aside)


