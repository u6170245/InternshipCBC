+ sep 20 list all eventbrite api
+ sep 27 lamp stack, wp admin account of cbc, ftp account of cbc, no shell access of cbc's server.
+ sep 29 restart server when boot up
	```
	sudo apache2ctl configtest
	sudo systemctl restart apache2.service
	```
+ sep 29 give php permission to path. (in creating, writing, uploading files)
	```
	sudo chown -R www-data /path/html/
	sudo chown -R www-data:www-data /path/html/
	```
+ sep 29. upload file to server
	- call `phpinfo()`, or write a php file with `phpinfo()` and run it on browser.
	- "Loaded Configuration File" show the location of php.ini
	- `file_uploads = On` in php.ini
	- file size
		+ `upload_max_filesize = 2M` this is the max size of file uploaded
		+ in apache config file
			```
			<Directory "/var/www/vhosts/path/to/your/directory/import/">
			    php_value post_max_size 6M
			    php_value upload_max_filesize 6M
			</Directory>
			```
+ sep 29. I have give permission of /path to www-data with option -R. after that, I create a new folder /path/uploads/. I have to give permission of this new folder. Apache will not have permission of that path automatically.

+ oct 1. php can work with both mysqli and pdo. the former only works for mysql. the later can work for 12 different dbms.
+ oct 1. `grep -R "new mysqli" *` and `grep -R "new PDO" *` both return non-empty results. so wp template of cbc uses both package.
+ install and config mysqli and pdo
	- `apt-get install php-mysql` or `php7.0-mysql` on a ubuntu 16.04.
	- edit `/etc/php/7.0/apache2/php.ini`, umcomment `extension=php_pdo_mysql.so` and/or `extension=php_mysqli.so`
	- `service restart apache2` 
	- check whether it is available in `phpinfo()`
	- alternative for PDO, `sudo phpenmod pdo_mysql`

+ oct 1. set up mysql. 
	- if it is run on Debian, it is not neccessary to run `sudo mysqld --initialize`
	- `systemctl status mysql.service`. 
	- if MySQL isn't running, `sudo systemctl start mysql`
	- connect to MySQL as admin. `mysqladmin -p -u root version`	
	- `mysql --user=root --password` to run in an interactive interface in shell
	- `CREATE DATABASE test;` then `SHOW DATABASES`.
	- `mysql --user=root --password hbTest`

+ oct 1, xml parser in php
	- tree based. simplexml, dom
	- event based. read in one node at a time. xmlreader, xml expat parser.
	- for cbc event calendar. tree based is enough. I will use simplexml
	- install `sudo apt-get install php7.0-xml`


+ oct 4. there are two kinds of workflow. One is using a (I) Personal Token, the other is using a (II) OAuth Token Flow. For the former one, one just do an integration for a single user or organization. For the later one, one is using eventbrite api for many eventbrite users (service building on top of eventbrite). CBC event calendar is something like the second situation. But I don't know which one is more suitable at present. So today, I tried the first workflow. I might try the second in next week.

+ oct 4, a test account on eventbrite. Register - Create APP - Find keys. Details see [here](https://www.eventbrite.com/developer/v3/api_overview/authentication/)
	- username: u6170245@anu.edu.au. pwd: my ANU password.
	- login - account setting - app management - create a new app. then fill in the form
	- The form: "Hongbo Zhang" "https://www.canberrabusiness.com/events/" "CBCECTest" "Canberra Business Center International Trade Event Calendar Test"
	- click CREATE KEY
	- keys are show in app management page, and you can update details of CBCECTEST app here
		+ Non-secret Client Key 6OY5PASAZ3HH6AH2R7 (API app key)
		+ click "Show" to show secret client key and personal oauth token. They are secret.

+ In the First workflow (I), personal OAUTH token is enough.

+ Eventbrite limits 1000 call per hour on each OAuth token. but that's far far enough for CBC.

+ Two ways to authentication requests.
	- `Authorization: Bearer $(personal oauth token)` in authentication header
	- query string parameter. `https://www.eventbriteapi.com/v3/users/me/?token=$(personal oauth token)`

+ SDK of eventbrite API for PHP, see [here](https://github.com/eventbrite/eventbrite-sdk-php)

+ testing api `/users/me/owned_events/` and `/users/me/` from browser
+ testing api `/users/me/owned_events/` by php
	- my own http request code in */practice/www/html/cbcectest/allEvents.php*
	- using sdk in */practice/www/html/cbcectest/eventbrite-sdk-php/tests/allEvents.php*
+ Notice that in testing by sdk, I call `get_user_owned_events('me')`, but return error responds (400). The error message said ***You passed a request body that was not in JSON format.***. This bug will be removed by commenting out line 49 in `HttpClient.php` in SDK-php

+ set up wordpress. see [here](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-ubuntu-14-04)
	- database and user. 
		```
		mysql -u root -p
		CREATE DATABASE wordpress;
		CREATE USER hongbo@localhost IDENTIFIED BY 'password';	
		GRANT ALL PRIVILEGES ON wordpress.* TO hongbo@localhost;
		FLUSH PRIVILEGES;
		```
	- download wordpress `wget http://wordpress.org/latest.tar.gz`. extract it. and install some libraries `sudo apt-get install php5-gd libssh2-php`
	- configure wordpress. set the path to document root of apache. finalize by web interface.


+ 0ct-09. discuss with Steven. 
	- I can put the source code on the gitlab, as long as other will not hack the website by reading the code. So I am afraid I should not put the source code on gitlab.
	- the payment problem. If we create an account CBCEC on eventbrite, and all the events were pushed on the eventbrite under CBCEC, what about the paying for the ticket? All the ticket fee will go to CBC's bank account, and CBC should transfer money back to event holder. Steve and I discuss about this issue, if each eventbrite account can have more than one bank account, this problem is solved. If the number is limited (e.g., 10), we will roll the bank accounts, since typically, there will no too much events.
	- I have checked, eventbrite allows multiple bank accounts. See [here](https://www.eventbrite.com.au/support/articles/en_AU/How_To/how-to-set-up-event-payout-details?lg=en_AU)
		```
		PRO TIP: You have the option of giving your bank account a nickname. This is especially useful if you use multiple bank accounts and want to easily keep track of them.
		NOTE: Payout details are entered on a per-event basis and there isn't a way to apply your payout details account-wide to each event created under your Eventbrite account. 
		```
+ Oct-10. A good introduction to Wordpress development is [here](https://premium.wpmudev.org/blog/wordpress-development-beginners-getting-started/)
