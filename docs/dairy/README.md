This is a working dairy.

# Jul 26
+ first time to CBC, met all the colleagues in CBC. there are around 20 employees here. Robin is CEO. I only met one male here.
+ Erika told me what the project is. 
CBC makes extensive use of third party IT services to facilitate their business.
For example, they use Infusionsoft to deal with their CRM, and Xero as their financial system. 
However, these third party services don't talk to each other.  
It will bring inconenvient to their business.
Especially, when the members pay their membership fee annually, 
staff of CBC has to export their member info from CRM system, and input them to financial system manually to generate invoices.
So they want this whole process automatically. 
This project only concern about generating invoices.
+ We check the feasibility of Infusionsoft API. Infusionsoft provides rich APIs which can satifisy the requirements of CBC

# Jul 31
+ CBC is a private, non-profit company. It is like a labor union for companies.
It provides financial services/consultants for them. They also communicates with
governemnt as a representative of companies. It has around 600 members.
+ we discussed the project in details, including which information should be input in UI, which info is required to extract from CRM database, the necessarity to check there is no duplication in extracted data due to the bad design of CRM database, the differences between WR type companies and other type companies, which info should be feed into financial system, which emails should be sent after finishing whole processes. I made a record on paper
+ after dicussion. 
	- UI conceptual sketch
	- backbone, walking skeleton
	- releases plan
+ a commercial plugin already existed, costing 500 dollar per month.

# Aug - 2
+ Erika told me project might be changed, since intern should not have permission to their CRM and financial database.
+ They will hold a meeting with Adrain in this afternoon
+ Erika told me I could leave the office, and come back when the scope is determined.

# Aug - 3
+ Erika told me that we, including CBC, my mentor and Adrain will have a meeting on next Wednesday on the scope of this project

# Aug - 7 
+ concept review

# Aug - 8
+ The scope meeting with CBC, mentor and Adrain is rescheduled to next Monday.

# Aug - 9 
+ mentor meeting at cafe of Llenyel hall.
	- Alaine told me they have a brief discussion on my project. they give me two options, one is continuing previous project in a testing environment (sandbox), the other is beginning a new one, which is building an international trading calendar.
	- next mentor meeting will be held on 25 Aug at ANU sport cafe, it is a individual face to face meeting 

# Aug - 14
+ come back to office. 
+ discuss with Erika on the new project, a trading calendar
+ the motivation of new project. CBC signed a contract with governemnt on becoming a leader in its area. in the contract, it requires CBC building its own trading calendar. That's why CBC wants to build a new calendar, rather than using existed one, such as eventbrite, tradeevents, austrade and so on.
+ this calendar is highly similar to eventbrite, but only focus on *international trade in Australia*.
+ simple description of the new project.  In a trading calendar, everyone, rather than CBC and its member only, can create, save, edit and submit an event related to international trade which is held in Australia (not restricted to Canberra), including workshop, seminar, networking, webinar and so on.  After submitting, staff at CBC will approve, update or reject the event. However, if approved, this event will be put on trading event website, and anyone can search them.
+ I decided to take the new project
	- the old one has permission issue, it will be built only in sandbox
	- there has already existed commercial plugin, which costs only 500 dollar per month, to solve the problem. If I build a new one, the potential cost of maintaining in the future might be even higher, since they don't have any IT staff.
	- personal reason, the new one is much more challenge.
+ discuss with Alaine, 
	- determining do the trading calendar project. 
	- she suggested me holding scrum review regularly, although there are only me in dev team. I agreed with her.
	- she agrees to attend the scrum review and wears a hat in that review, if she can.
	- she suggests I can spend about two weeks or more on initial concept design period, including making user story map, design database schema, design architecture and so on.
+ meeting with Alaine, Erika, Kate and Adrain (absent)
	+ determining how many roles in this project, which can be found in [here](../design/userStory/roles.md)
	+ making an agreement the format of user stories, i.e., using *As a ..., I want ..., so that ...* format.
+ Ask Roxanne for documents of UI of CBC website. She gave me color sheet, and full access permission to the backend of CBC website, so that I can read all the css by myself.
+ 8755 tutorial
	- feedback of conceptual review. I should add more details in dairy on decision making and client's consensing in roadmap I built.
	- we should take the fact that there is no IT staffs into consideration. since after we leave, who maintain this system. maintainance may be expensive.
	- we should not use the tools which is free for students only. 

# Aug - 15
+ Meeting with Shanye and Ramesh
	- project changing are very common, that is good experience for me.	
	- I should record all the details of issues, events, decision, including the previous project, in my dairy, even if the project changes.
	- I should spend definitely more than 2 weeks in concept design period. A correct unfinished work will bring more value than an incorrect finished work.
	- I must have a user story validation process before designing database. I should ask Erika to find someone who will be Event Owner and discuss them face to face.
	- Shanye taugth me how to perfomr a user story validation, I should prepare some documents (such as UI of website) before the meeting, and in the meeting, I don't show them those documents at first. I should ask for their concern and their ideas, make records, draw their ideas on paper by hand while discussion, and in the end, show them the prepared documents.
	- Shanye said maybe I can build a system using eventbrite! since there is no IT guys in CBC, so if I build everything from scratch, their cost for maintaining it after my leaving will be huge. However, if I build this system by using some simple js basing on eventbrite, it will be much cheaper to maintain. Since everyone can do js.
	- the calendar of ANU is buiding basing on eventbrite by some simple js

# Aug - 16
+ Both Erika and me wrote a bunch of user stories.
+ I organize those stories, and can be found in pdf format [story170816.pdf](../design/userStory/story170816.pdf)
+ I ask Erika to find some member of CBC acting as "Event Owner" in user story validation, Erika thinks we can invite staffs in CBC acting this role in story validation meeting, becausethe "Event Owner" can be any one who organize trading event, rather than their member only. Hence their staffs can act this role. I suggest her to find some one who actually submits event, since it is about how user uses the system, rather than UI only. She thinks their staffs are fine. So in next Wednesday or next next Monday, there will be a user story validation meeting.
+ Erika confirms that this calendar will not involve anything related to managing the event. e.g., some event may require governemnt/organization agreement, such as fire control department, police department and so on. However, administrator of this calendar will not check whether they have them.
+ Erika confirms that there is no requirements of *report* in the contract with governemnt on this calendar website. so currently, just leave it aside.

# Aug - 18
+ Mentor meeting with Alaine.
	- there are two options to the solution, S1 building from scratch, S2 ANU solution (js + eventbrite). I have difficulty to make a decision. Alaine helps me to clarify the benefit and drawback of each options. Finally, I choice S2, since it seems that the benefit of S1 is too small for both me and my client.
	- In the next, I will ask Ramesh to redirect me to IT department of ANU on S2 (Aug 21 - 25), making a slide/demo at Aug 26, discuss stories and build MVP at Aug 21, and show them to CBC at Aug 26.
	- told me differences between solution and product.
	- speak slow to business people


# Aug - 21
+ Plan to discuss user stories with Erika and obtain MVP from the discussion. Unfortunately, Erika is sick today.
+ Look through ANU event. 
	- Its source is not restricted eventbrite only. 
	- It seems that all the events from eventbrite are holded or partially hold by ANU. 
+ In the tutorial, I told my concern of ANU calendar to Peter and Shanye. They asked me to send email to Shanye and cc to Peter. They will ask IT department for who responds for ANU event project.

# Aug - 23
+ discuss user stories with Erika. 
	- think through stories one by one 
	- clarify some details and record them in [user story](../design/userStory/)
	- prioritize these stories.
+ extract MVP from those stories. Of course, it is a draft version, which need further discussion and validation.
+ make a user story map, which can be found in pdf format [story170823.pdf](../design/userStory/userMap170823p.pdf)
+ in the process of making user map, I put some new stories (brand new ones or new stories from decomposition of big stories) into file [nonMVP](../design/userStory/nonMVP170823.md)
+ Erika said my product is just a prototype. They are hiring a programm manager, and that manager will make final decision of Event-calendar product basing on this prototype. The recuitment of programm manager may be last for severl months.


# Aug - 28
+ drop in the IT support department at Braid Anderson Building to ask for experience of ANU event calendar. They told me the ANU event website is not made and maintained by them.
+ I email events@anu.edu.au for detailed information, and is waiting them for reply. If they won't, I will drop in.

# Aug - 29
+ some possible solution of integrating CBC and eventbrite
	1. CBC create a unique account, and all the users of this website will publish their events on eventbrite using this unique account
	2. Any user who wants to publish an event will register an account on eventbrite, and CBC will decide which event is displayed on CBC's website.
+ study eventBrite API and check whether it matches with MVP. details see [here](../design/tech/eventbriteAPI.md)
+ some technical decision.
	- don't use DBMS to store user registeration info. details see [here](../design/tech/eventbriteAPI.md)

# Sep - 18
+ Erika will leave CBC in next week. So she introduced me to Steven and Dinko. Steven responds for finance and "covers" IT part. Dinko works on international trade. I will report to them afterwards.
+ Erika said Adrain told them I am in the role of consultant here in CBC, rather than an intern (which confuses me a lot). I was required to write a consultant report before the end of this internship.


# Sep - 20 
+ Eventbrite API see [here](https://www.eventbrite.com/developer/v3/#ebapi-endpoints)
## About register

+ when a user registers an account, we write them into a txt file on server. 
+ It is not necessary to use DBMS at present stage, since the number of users should not too much. (there are only 600 members for CBC, even we amplify this number by 100, it is 60 thousands, which is not too much at all)
+ if we use DBMS, we have to install DBMS on server of CBC. However, their server is maintained by 3rd party company. It will be inconenvient for us to config, develop, test, debug. 
+ As well, it will cost CBC money to buy data storage service. But currently, this is only a prototype. 
+ If the number of users increase greatly, we will transfer to DBMS at that time.

## About login

+ when a user logs in, check whether he is in txt file.

## About update user profile

+ update txt file

## About creating event

+ fill in a form
+ `POST /events/` for creating new event

## About viewing event

+ list all events of CBCEC with specific event organiser fields.
+ `https://www.eventbriteapi.com/v3/users/me/owned_events/?token=MYTOKEN1`
+ there is a field `status` in responds json to tell whether is live or not. For us, it tells an event if submitted or approved

## About updating event

+ in viewing event page, user clicks an update button on an event
+ he can fill in a form. 
+ then submit this form, and change correspondence fields in eventbrite event.
+ `POST /event/:id/` to update an event

## About deleting event

+ in viewing event page, user clicks a delete button on an event.
+ he can delete an event
+ `POST /events/:id/cancel/`
+ `DELETE /event/:id/`

## About submit

+ when click submit button, this form is ***submitted to eventbrite by account CBCEC*** without "Make Event Live" 
+ `POST /events/:id/publish/` 
+ CBC admin should set it to public, rather than private.

## About emails when any operation (submit, update, approve, reject) happens

+ TODO does eventbrite have this function? eventbrite only has invitation function when the an event goes alive.

## About viewing and searching events by attendee

+ viewing all events of CBCEC.
+ `https://www.eventbriteapi.com/v3/users/me/owned_events/?token=MYTOKEN1` with `status` doesn't euqal to `live`

## About managing events by Admin

+ just login eventbrite by CBCEC, and do stuff

# Sep - 27 
+ lamp stack, wp admin account of cbc, ftp account of cbc, no shell access of cbc's server.

# Sep - 29 
+ restart server when boot up
	```
	sudo apache2ctl configtest
	sudo systemctl restart apache2.service
	```
+ give php permission to path. (in creating, writing, uploading files)
	```
	sudo chown -R www-data /path/html/
	sudo chown -R www-data:www-data /path/html/
	```
+ upload file to server
	- call `phpinfo()`, or write a php file with `phpinfo()` and run it on browser.
	- "Loaded Configuration File" show the location of php.ini
	- `file_uploads = On` in php.ini
	- file size
		+ `upload_max_filesize = 2M` this is the max size of file uploaded
		+ in apache config file
			```
			<Directory "/var/www/vhosts/path/to/your/directory/import/">
			    php_value post_max_size 6M
			    php_value upload_max_filesize 6M
			</Directory>
			```
+ I have give permission of /path to www-data with option -R. after that, I create a new folder /path/uploads/. I have to give permission of this new folder. Apache will not have permission of that path automatically.

# Oct - 1
+ php can work with both mysqli and pdo. the former only works for mysql. the later can work for 12 different dbms.
+ `grep -R "new mysqli" *` and `grep -R "new PDO" *` both return non-empty results. so wp template of cbc uses both package.
+ install and config mysqli and pdo
	- `apt-get install php-mysql` or `php7.0-mysql` on a ubuntu 16.04.
	- edit `/etc/php/7.0/apache2/php.ini`, umcomment `extension=php_pdo_mysql.so` and/or `extension=php_mysqli.so`
	- `service restart apache2` 
	- check whether it is available in `phpinfo()`
	- alternative for PDO, `sudo phpenmod pdo_mysql`

+ set up mysql. 
	- if it is run on Debian, it is not neccessary to run `sudo mysqld --initialize`
	- `systemctl status mysql.service`. 
	- if MySQL isn't running, `sudo systemctl start mysql`
	- connect to MySQL as admin. `mysqladmin -p -u root version`	
	- `mysql --user=root --password` to run in an interactive interface in shell
	- `CREATE DATABASE test;` then `SHOW DATABASES`.
	- `mysql --user=root --password hbTest`

+ xml parser in php
	- tree based. simplexml, dom
	- event based. read in one node at a time. xmlreader, xml expat parser.
	- for cbc event calendar. tree based is enough. I will use simplexml
	- install `sudo apt-get install php7.0-xml`

# Oct - 3
+ Discuss with Alaine about the structure of poster.

# Oct - 4
+ there are two kinds of workflow. One is using a (I) Personal Token, the other is using a (II) OAuth Token Flow. For the former one, one just do an integration for a single user or organization. For the later one, one is using eventbrite api for many eventbrite users (service building on top of eventbrite). CBC event calendar is something like the second situation. But I don't know which one is more suitable at present. So today, I tried the first workflow. I might try the second in next week.

+ a test account on eventbrite. Register - Create APP - Find keys. Details see [here](https://www.eventbrite.com/developer/v3/api_overview/authentication/)
	- username: u6170245@anu.edu.au. pwd: my ANU password.
	- login - account setting - app management - create a new app. then fill in the form
	- The form: "Hongbo Zhang" "https://www.canberrabusiness.com/events/" "CBCECTest" "Canberra Business Center International Trade Event Calendar Test"
	- click CREATE KEY
	- keys are show in app management page, and you can update details of CBCECTEST app here
		+ Non-secret Client Key 6OY5PASAZ3HH6AH2R7 (API app key)
		+ click "Show" to show secret client key and personal oauth token. They are secret.

+ In the First workflow (I), personal OAUTH token is enough.

+ Eventbrite limits 1000 call per hour on each OAuth token. but that's far far enough for CBC.

+ Two ways to authentication requests.
	- `Authorization: Bearer $(personal oauth token)` in authentication header
	- query string parameter. `https://www.eventbriteapi.com/v3/users/me/?token=$(personal oauth token)`

+ SDK of eventbrite API for PHP, see [here](https://github.com/eventbrite/eventbrite-sdk-php)

+ testing api `/users/me/owned_events/` and `/users/me/` from browser
+ testing api `/users/me/owned_events/` by php
	- my own http request code in */practice/www/html/cbcectest/allEvents.php*
	- using sdk in */practice/www/html/cbcectest/eventbrite-sdk-php/tests/allEvents.php*
+ Notice that in testing by sdk, I call `get_user_owned_events('me')`, but return error responds (400). The error message said ***You passed a request body that was not in JSON format.***. This bug will be removed by commenting out line 49 in `HttpClient.php` in SDK-php

+ set up wordpress. see [here](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-ubuntu-14-04)
	- database and user. 
		```
		mysql -u root -p
		CREATE DATABASE wordpress;
		CREATE USER hongbo@localhost IDENTIFIED BY 'password';	
		GRANT ALL PRIVILEGES ON wordpress.* TO hongbo@localhost;
		FLUSH PRIVILEGES;
		```
	- download wordpress `wget http://wordpress.org/latest.tar.gz`. extract it. and install some libraries `sudo apt-get install php5-gd libssh2-php`
	- configure wordpress. set the path to document root of apache. finalize by web interface.


# Oct - 9
+ discuss with Steven. 
	- I can put the source code on the gitlab, as long as other will not hack the website by reading the code. So I am afraid I should not put the source code on gitlab.
	- the payment problem. If we create an account CBCEC on eventbrite, and all the events were pushed on the eventbrite under CBCEC, what about the paying for the ticket? All the ticket fee will go to CBC's bank account, and CBC should transfer money back to event holder. Steve and I discuss about this issue, if each eventbrite account can have more than one bank account, this problem is solved. If the number is limited (e.g., 10), we will roll the bank accounts, since typically, there will no too much events.
	- I have checked, eventbrite allows multiple bank accounts. See [here](https://www.eventbrite.com.au/support/articles/en_AU/How_To/how-to-set-up-event-payout-details?lg=en_AU)
		```
		PRO TIP: You have the option of giving your bank account a nickname. This is especially useful if you use multiple bank accounts and want to easily keep track of them.
		NOTE: Payout details are entered on a per-event basis and there isn't a way to apply your payout details account-wide to each event created under your Eventbrite account. 
		```
# Oct - 11
+ A good introduction to Wordpress development is [here](https://premium.wpmudev.org/blog/wordpress-development-beginners-getting-started/)

# Oct - 15
+ Alaine revise my CV.
